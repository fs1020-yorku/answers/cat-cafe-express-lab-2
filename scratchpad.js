let cats = [
  {
    id: 1,
    name: "Tuna",
    breed: "Siamese",
  },
  {
    id: 2,
    name: "Chester",
    breed: "Tabby",
  },
  { id: 3, name: "Blue", breed: "Naked" },
];

// const index = cats.find((cat) => cat.id == 2);
// console.log(index);

const index = cats.findIndex((cat) => cat.id == 3);
console.log(index);
